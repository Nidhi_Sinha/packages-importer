package gitlab

import (
	"fmt"
	"strings"
)

// PipelineJob represents a GitLab pipeline job.
type PipelineJob interface {
	Label() string // Returns the label of this job.
}

// Pipeline represents a GitLab pipeline configuration, which can be summarized by a
// collection of jobs.
type Pipeline struct {
	Stages []string
	Jobs   map[string]PipelineJob `yaml:",inline"`
}

// AddHiddenJob adds a hidden job definition to the pipeline. That job is defined by
// a stage, an image and a set of scripts. See https://docs.gitlab.com/ee/ci/jobs/#hide-jobs
func (p *Pipeline) AddHiddenJob(stage, image string, scripts []string) error {
	label := p.hiddenJobLabel(stage)
	hiddenJob, err := newHiddenJob(label, stage, image, scripts)
	if err != nil {
		return err
	}

	p.addPipelineElement(hiddenJob)

	return nil
}

// AddJob adds a standard job. Since that job is specialized for a package import, it is defined by
// a stage, an image, a package name and a version.
func (p *Pipeline) AddJob(stage, image, packageName, packageVersion string) {
	variables := p.packageVariables(packageName, packageVersion)
	label := fmt.Sprintf("%s:%s:%s", stage, packageName, packageVersion)
	job := newJob(label, p.hiddenJobLabel(stage), variables)

	p.addPipelineElement(job)
}

func (p *Pipeline) addPipelineElement(pj PipelineJob) {
	p.Jobs[pj.Label()] = pj
}

func (p *Pipeline) hiddenJobLabel(stage string) string {
	return fmt.Sprintf(".%s:scripts", stage)
}

func (p *Pipeline) packageVariables(pkgname, pkgversion string) map[string]string {
	return map[string]string{
		"PACKAGE_NAME":    pkgname,
		"PACKAGE_VERSION": pkgversion,
	}
}

func newPipeline(stageSize, jobsSize int) *Pipeline {
	return &Pipeline{
		Stages: make([]string, 0, stageSize),
		Jobs:   make(map[string]PipelineJob, jobsSize),
	}
}

// Job represents a job for a GitLab pipeline. Since the scripts are centralized in an
// job, this structure only needs to define the environment variables that those scripts need.
type Job struct {
	label     string
	Extends   string
	Variables map[string]string
}

// Label returns the job's label.
func (j Job) Label() string {
	return j.label
}

func newJob(label, extends string, variables map[string]string) Job {
	return Job{
		label:     label,
		Extends:   extends,
		Variables: variables,
	}
}

// HiddenJob represents a hidden job for a GitLab pipeline. See https://docs.gitlab.com/ee/ci/jobs/#hide-jobs.
// Hidden jobs will mainly host the scripts lines that need to be executed to import a package.
type HiddenJob struct {
	label   string
	Image   string
	Stage   string
	Needs   []string
	Scripts []string `yaml:"script"`
}

// Label returns the hidden job's label.
func (j HiddenJob) Label() string {
	return j.label
}

func newHiddenJob(label, stage, image string, scripts []string) (HiddenJob, error) {
	if !strings.HasPrefix(label, ".") {
		return HiddenJob{}, fmt.Errorf("error with label %q: hidden jobs labels must start by a dot(.)", label)
	}

	return HiddenJob{
		label:   label,
		Stage:   stage,
		Image:   image,
		Needs:   []string{},
		Scripts: scripts,
	}, nil
}
